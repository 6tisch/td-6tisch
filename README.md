# 6TiSCH Interoperability Test Description

Set of interop tests for 6TiSCH. Used in

* face-to-face interop events:
    * [2nd F-Interop 6TiSCH], (http://www.etsi.org/news-events/events/1247-6tisch-f-interop-paris-2018), Paris, France, 26-27 June 2018
    * [1st F-Interop 6TiSCH], (http://www.etsi.org/news-events/events/1197-6tisch-interop-prague-2017), Prague, Czech Republic, 14-15 July 2017
    * [6TiSCH/6lo Plugtest] (http://www.etsi.org/news-events/events/1077-6tisch-6lo-plugtests), Berlin, Germany, 15-17 July 2016
    * [6TiSCH Plugtests 2] (http://www.etsi.org/news-events/events/1022-6TiSCH-2-plugtests), Paris, France, 02-04 February 2016
    * [6TiSCH Plugtests](http://www.etsi.org/technologies-clusters/technologies/testing/10-news-events/events/942-6tisch-plugtests), Prague, Czech Republic, 17-18 July 2015
* [F-Interop](http://www.f-interop.eu/) online testing platform.

## repo organization

* Documentation:
    * 01-preface.md
    * 02-intro.md
    * 03-abbreviations.md
    * 04-conventions.md
    * 05-config.md
    * 06-golden_device.md
    * 07-f-interop.md
* Test Description:
    * tests_a_syn.yml
    * tests_b_minimal.yml
    * tests_c_l2sec.yml
    * tests_d_secjoin.yml
    * tests_e_6lorh.yml
    * tests_f_6p.yml
* PDF generation:
    * Rakefile
    * insert_version.py
* README
    * README.md
    
## get PDF

### latest build

https://openwsn-builder.paris.inria.fr/job/td-6tisch%20builder/

### generate from source

#### without Docker

Assuming you use a Ubuntu machine:

```shell
$ sudo apt-get install ruby ruby-dev rake ruby-bundler ruby-haml python
pandoc xvfb pdftk wkhtmltopdf ghostscript enscript
$ git clone https://bitbucket.org/6tisch/td-6tisch/src/master/
$ cd td-6tisch
$ rake
```

#### with Docker

```shell
$ git clone https://bitbucket.org/6tisch/td-6tisch/src/master/
$ cd td-6tisch
$ docker pull yatch/td-6tisch-builder
$ docker run -v `pwd`:/home/user/td-6tisch -ti yatch/td-6tisch-builder rake
```

Running `make` is equivalent to the last command shown above:

``` shell
$ make
```
