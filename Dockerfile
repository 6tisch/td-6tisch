FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y ruby ruby-dev rake ruby-bundler ruby-haml python pandoc xvfb pdftk

# install wkhtmltopdf via the official release since its Ubuntu
# package is old and doesn't work as expected, for instance, links in
# a generated pdf file are not clickable.
RUN apt-get install -y wget xz-utils libxrender1 libfontconfig1
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN tar -xf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
RUN cp wkhtmltox/bin/wkhtmltopdf /usr/bin/wkhtmltopdf
RUN chmod a+x /usr/bin/wkhtmltopdf
RUN rm -rf wkhtmltox

# need ttf-mscorefonts for a mono space font. however the ubuntu
# package of ttf-mscorefonts-installer fails to be installed.
# use the debian package following  https://ubuntuforums.org/showthread.php?t=2349320
RUN wget http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb
RUN apt-get -y install gdebi
RUN yes | gdebi ttf-mscorefonts-installer_3.6_all.deb
RUN rm ttf-mscorefonts-installer_3.6_all.deb

# they are for adding page numbers
RUN apt-get -y install ghostscript enscript

RUN mkdir -p /home/user
RUN mkdir /home/user/.enscript
COPY footer.hdr /home/user/.enscript/
# use specific user id and group id which are used by the builder
RUN groupadd --gid 130 user
RUN useradd --uid 122 -d /home/user -g sudo user
RUN chown user /home/user

USER user
WORKDIR /home/user/td-6tisch
