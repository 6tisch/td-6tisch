# This is a helper Makefile for docker users
# Note that rules for PDF generation are defined in Rakefile

all:
	docker run -v `pwd`:/home/user/td-6tisch -ti yatch/td-6tisch-builder rake

clean:
	docker run -v `pwd`:/home/user/td-6tisch -ti yatch/td-6tisch-builder rake clean
