## IEEE802.15.4 Default Parameters

All the tests are performed using the following setting.

### Address length

ALL IEEE802.15.4 addresses will be long (64-bit). The exception is broadcast address:

    Broadcast Address: 0xffff.

### Frame version

ALL IEEE802.15.4 frames will be of version 2 (b10).

### PAN ID compression and sequence number

All IEEE802.15.4 frames will contain the following fields:

- a source address,
- a destination address,
- a sequence number,
- a destination PANID (no source PANID).

### Payload termination IE

The IE payload list termination will NOT be included in the EB.

### IANA for 6P IE related

Since they have not been defined yet by IANA, for the Interop event, we use the following values:


| Abbreviation              | Value   |
| ------------------------- | ------- |
| IANA_IETF_IE_GROUP_ID     | 0x05    |
| IANA_6TOP_SUBIE_ID        | 0xC9    |
| IANA_6TOP_6P_VERSION      | 0x00    |
| IANA_SFID_SF0             | 0x00    |
| IANA_6TOP_CMD_ADD         | 0x01    |
| IANA_6TOP_CMD_DELETE      | 0x02    |
| IANA_6TOP_CMD_RELOCATE    | 0x03    |
| IANA_6TOP_CMD_COUNT       | 0x04    |
| IANA_6TOP_CMD_LIST        | 0x05    |
| IANA_6TOP_CMD_SIGNAL      | 0x06    |
| IANA_6TOP_CMD_CLEAR       | 0x07    |
| IANA_6TOP_RC_SUCCESS      | 0x00    |
| IANA_6TOP_RC_EOL          | 0x01    |
| IANA_6TOP_RC_ERROR        | 0x02    |
| IANA_6TOP_RC_RESET        | 0x03    |
| IANA_6TOP_RC_VER_ERR      | 0x04    | 
| IANA_6TOP_RC_SFID_ERR     | 0x05    |      
| IANA_6TOP_RC_SEQNUM_ERR   | 0x06    |
| IANA_6TOP_RC_CELLLIST_ERR | 0x07    |
| IANA_6TOP_RC_BUSY         | 0x08    |
| IANA_6TOP_RC_LOCKED       | 0x09    |

### Slotframe length.

Unless otherwise stated, the slotframe length is set to 101 slots.

### RPL Operation Mode

We use the Non-Storing mode during the tests.

### Default Layer-2 Security Keys

To perform the SEC-related tests, the value of keys K1 and K2 is the same and will be set to:

    K1 and K2 value: 0x11111111111111111111111111111111

Moreover, Key Index (advertised in the auxiliary security header of the packet) is used to enable nodes to look up the right key before decrypting.

    Key Index value for K1 and K2: 0x01

## Secure Join Default Parameters

Secure join (SECJOIN) tests are performed using the following common parameters and configuration settings.

### Configuration

JRC is co-located with DR.

### Link-layer Requirements

#### EB Period

For all SECJOIN tests, the DR sends EBs periodically, with a fast rate (equal to 10 sec, according to IEEE802.15.4std), so that the pledge does not need to send KAs for keeping synchronization.

#### Channel Hopping

All frames are sent on a single frequency -- channel hopping is disabled.

#### Layer 2 Security

The Layer 2 SEC option is enabled on DR and pledge.

The key K1/K2 is set as specified before. They are unknown to the pledge.

### Common Security Context

JRC and pledge share an OSCORE security context established out-of-band.

The Master Secret (MS) is set to:

    MS: 0xDEADBEEFCAFEDEADBEEFCAFEDEADBEEF

All the other parameters used for OSCORE, such as the identifiers, algorithm, the hash function, the key derivation function, are implemented as per draft-ietf-6tisch-minimal-security-06.

### Resources Exposed by JRC

As per draft-ietf-6tisch-minimal-security-06, JRC runs a CoAP (RFC7252) server exposing /j resource, that is OSCORE protected with POST as the only allowed method.

For successful join requests to /j resource, JRC returns a CBOR object, as per draft-ietf-6tisch-minimal-security-06.
The returned CBOR object is instantiated by the JRC so that it returns a single K1/K2 key, as specified above.
Since the JRC is co-located with DR, JRC does not return its IPv6 address in the join response.
The JRC does not include the short address field in the response.

### IANA for CoAP Options

Since they have not been defined yet by IANA, for the Interop event, we use the following values:

| Description                 | Value   |
| --------------------------- | ------- |
| OSCORE CoAP option number   |   21    |
| Stateless-Proxy CoAP option |   40    |

