# coding: utf-8

import re
from datetime import datetime

regex = re.compile(r"Version: (.)*", re.MULTILINE)
current_date = str(datetime.now())

lines = []

with open("01-intro.htm") as f:
    lines = f.readlines()

with open("01-intro.htm", "w") as f:
    for line in lines:
        line = regex.sub("Version: %s" % current_date, line)
        f.write(line)
