﻿# 6TiSCH Interoperability Test Description
***For 2nd F-Interop 6TiSCH Interoperability Event***

Version: VERSION_STRING

This document is the Test Description (TD) for the second F-Interop 6TiSCH Interoperability event, organized by [ETSI](http://www.etsi.org) and [LIST](https://www.list.lu/), in the framework of the SORT project, funded by the [H2020 F-Interop open call](https://ec.europa.eu/programmes/horizon2020/).

The Interoperability event will be held on 26-27 June 2018, in Paris, France, hosted by Inria.

The participants will test their implementation using the 6TiSCH online tools developed by Inria, and also by UCG, in the framework of the [SPOTS project](http://spots.ac.me). 
This document contains the 6TiSCH tests, made available on the F-Interop Platform, and the guidelines on how to run these tests during the event.

The document is organised in six sections:

- [Abbreviations](#abbreviations)
- [How to use the F-Interop platform](#finterop-platform)
- [Golden Device and Sniffer](#tech-support)
- [Requirements for attendees](#requirements)
- [Definitions, Topology and Proforma](#definition)
- [Test description](#td)

<a name="abbreviations"></a>

## Abbreviations

The abbreviations used in this document are listed below.

| Abbreviation  | Meaning                         |
| ------------- | -------------                   |
| 6N            | 6TiSCH Node                     |
| 6P            | 6top Protocol                   |
| ACK           | Acknowledgment frame            |
| EB            | Enhanced Beacon frame           |
| DAG           | Directed Acyclic Graph          |
| IE            | Information Element             |
| JP            | Join Proxy                      |
| JRC           | Join Registrar/Coordinator      |
| KA            | Keep-Alive message              |
| NUT           | Node Under Test                 |
| RPI           | RPL Information Option          |
| TD            | Test Description                |

<a name="finterop-platform"></a>

## How to use the F-Interop platform
The F-Interop platform and tools will be presented in detail at the beginning of the event. Besides that, to allow participants to get familiar with the tool and ready for the event, we provide here guidelines on how to use the F-Interop Platform.
The F-Interop platform offers online interoperability and conformance testing tools for different IoT technologies, including 6TiSCH.
By loggin into the F-Interop Platform ([http://gui.f-interop.eu](http://gui.f-interop.eu)), a registered user has the access to the testing tools.
The tests related to the same technology belong to the same **Test Suite**.
To the scope of this event, the user has to select the **6TiSCH** Testing Tool as Test Suite, then select different categories of 6TiSCH Interoperability tests (minimal, 6P, security, etc.), shown up in a menu.
During the event, the user will select each time the specific Interoperability test that she wants to perform.

For the setup of the test, the user has to provide some information about the hardware and implementation under test, e.g. the board type, EUI64 address of the device, and the frequency selected for communication.
Each test is articulated in a sequence of steps. Detailed instructions on the actions to be taken in each step are provided in plain text, showing up on the web page.

After performing the different steps of the Interoperability test, the user will have to upload a pcap file to the F-Interop platform for the final check.
In fact, the final check (pass/fail) will be performed on the F-Interop platform with a built-in Wireshark 6TiSCH dissector.
If the built-in 6TiSCH dissector is able to dissect correctly all packets in the uploaded pcap file, a verdict with green frame is shown up.
If not, a red error message is shown up, indicating the reason of the failure.

Hereafter the sequence of steps required for performing a 6TiSCH test, using the F-Interop platform:

1. Register as a user on the F-Interop platform site and login.
2. Click "New test session" to start a new session.
3. Select "6TiSCH Testing tool" as Test Suite and click the gray right arrow button on the bottom.
4. Click "Save configuration" directly for an empty configuration and click the green "Start" button to start the test session. The following information will show up in the text box and the page will be redirect to session start-page.

        2018-04-10T07:07:20.442000+00:00 - info - Session created locally
        2018-04-10T07:07:21.016000+00:00 - info - Deploying session
        2018-04-10T07:07:21.110000+00:00 - info - Created user at Session Orchestrator
        2018-04-10T07:07:21.588000+00:00 - info - Created session at Session Orchestrator
        2018-04-10T07:07:21.588000+00:00 - info - Session deployed
        2018-04-10T07:07:21.735000+00:00 - info - Starting session
        2018-04-10T07:07:23.192000+00:00 - info - Session started

5. Select the test to perform through the dropbox and click "OK" button to start.
6. Follow the instruction showing up at each step and click "OK" button to continue.
7. A verdict will shows up to indicate whether the test passed or failed. In case the test fails, the verdict contains the reason of the failure.

<a name="tech-support"></a>

## Golden Device and Sniffer

During the 6TiSCH Interop Event, two devices are available for participants:

- A Golden Device implementation, which participants can use as a reference implementation while preparing for the event;
- A 16-channel IEEE802.15.4 sniffer  that will be installed during the event. It allows participants to follow the frames exchanges, and finally upload the pcap file into the F-Interop Platform.

### Golden Device

The Golden Device is an OpenMote-CC2538 programmed with a Golden Image containing the reference implementation of 6TiSCH.
The Golden Device allows users to test their implementation by testing against the reference implementation before the actual interoperability event.

The Golden Device can be programmed with two different type of Golden Images:

- With the first image, the Golden Device acts as DAGroot.
- With the second image, the Golden Device acts as 6N.

Both images can be configured using a script (described below), which allows setting the value of several parameters (e.g., frequency, slotframe size, etc.), or triggering the transmission of a given type of packet (EB, DATA, ACK, etc.).

By using the script, it is possible to configure:

- communication frequencies (Single frequency or Multiple Frequencies/Channel Hopping)
- slotframe size
- slot duration
- link-layer security (Enable/Disable)
- ACK reply (Enable/Disable)
- period of packet to send (EB, KA, DIO, DAO)
- issuing of any type of 6P request packet (6P_ADD, 6P_DELETE, 6P_RELOCATE, 6P_COUNT, 6P_LIST, 6P_CLEAR)

The script displays information about the frames the Golden Device sent/received from the vendor node.
For example, when the script issues a 6P packet, the Golden Device returns information about the 6P response (e.g. the number of reserved cells in a 6P_COUNT response, the reserved cell list in 6P_LIST response) sent by vendor node.
The value of the return code field in the 6P response is always printed.

The details of how to configure the Golden Image through this scripts can be found on the release of golden image source code page:

    https://github.com/openwsn-berkeley/openwsn-fw/releases


### 16 channel Sniffer 

The [Beamlogic 16 channel sniffer](http://www.beamlogic.com/802-15-4-siteanalyzer) will be used during the F-Interop 6TiSCH Interoperability event for sniffing all packets over the air, exchanged on the 16 channels.

A Zigbee Encapsulation Protocol (ZEP) header is added before the sniffered packets to information such as the length of the sniffed packet and on which channel it is received.
Those information will help when user performs a test.
All the sniffed packets including ZEP header are streaming to a MQTT server in the cloud.
An MQTT client allows the user to connect to the MQTT server to get the packets and browse in the wireshark.
Users will filter the packets according to PANID, and save the filtered results as a pcap file which is uploaded to the F-Interop platform and visualize the frame exchange using Wireshark.

Instructions on how to use the MQTT client are available here: 

    https://github.com/openwsn-berkeley/argus

### Wireshark

During the 6TiSCH Interoperability event, Wireshark will be used as reference dissector.
Users should use the latest version of Wireshark 2.9.0 or higher, which contains the dissector for 6TiSCH.
It can be downloaded from the latest build page:

    https://www.wireshark.org/download/automated/



<a name="requirements"></a>

## Requirements for attendees

### Physical attendees

During the 6TiSCH Interoperability event, the attendees need to bring the following devices to perform the interoperability tests:

- 4 vendor nodes, to perform multiple hop tests
- A computer able to run Wireshark, execute Python scripts and browse the F-Interop web server

Vendors should design a method / script to configure their nodes with a set of functionalities, similar to the configuration script used for the Golden Device.
The vendors will have to set up a specific configuration for their node, during the interoperability event, following the instructions received through the F-Interop platform.


### Remote attendees

For the vendors attending the event remotely, they MUST meet the same requirements for physical attendees.
Additionally, they MUST find a way to sniff the packets generated by their nodes and upload the pcap to the F-Interop platform.
This ability has been provided by the technical support team of the interoperability event.

<a name="definition"></a>

## Definitions, Topology and Proforma 

This section explains the definitions, topology and proforma that are used in the test description.

### Definitions

#### Synchronization & Time Correction

2 nodes are considered synchronized if and only if they can maintain a relative time drift lower than 40 [ppm](https://en.wikipedia.org/wiki/Parts-per_notation) from each other.

For example if two nodes do not re-synchronize 10s after having synchronized, their desynchronization must be below 400us:

    10 s * 40 ppm = 400 us

The Golden Device used in the interoperability events can provide time drift monitoring information obtained from the Keep Alive packets.
More information about this topic can be found [https://openwsn.atlassian.net/wiki/display/OW/Synchronization](https://openwsn.atlassian.net/wiki/display/OW/Synchronization).

#### Joining a network

A pledge successfully joined a network if and only if it performed the following steps successfully:

- It got time-synchronized
- It completed the security handshake and received a join response
- It received routing information, e.g obtaining a RPL Rank

#### Equipment Type:

- **DAGroot:** A DAGroot is a 6TiSCH Node acting as root of the routing DAG in the 6TiSCH network topology.
- **6TiSCH Node:** A 6TiSCH Node is any node within a 6TiSCH network other than the DAGroot.
- **Join Registrar/Coordinator (JRC):** central entity responsible for authentication and authorization of joining nodes (pledges).
- **Pledge:** A 6N, before having completed the join process.
- **Join Proxy (JP):** a stateless relay that provides connectivity between the pledge and the JRC during the join process.

Note that the 6TiSCH Node can act as parent and/or child node within the DAG.
It can communicate with its children and its parent using the 6TiSCH minimal schedule, or any other TSCH schedule.
In the test description, the term is used to refer to a non-DAGroot node.

### Topologies

To address different functional areas and groups of tests, the following topology scenarios have been defined.

#### Single hop

For most tests, the setup is a 6TiSCH single-hop topology, including a DAGroot and a 6TiSCH Node (6N).
In order to verify the correct formatting of the frames exchanged between DAGroot and 6TiSCH Node, a sniffer is used.


                   +----------------+
                   |                |
                   |    Sniffer     |
                   |                |
                   +----------------+


      +----------+                    +-------------+
      |          | +----------------> |             |
      | DAG root |                    | 6TiSCH Node |
      |          | <----------------+ |             |
      +----------+                    +-------------+


#### Multi-hop

The multi-hop scenario includes 1 DAGroot and 3 6TiSCH Nodes, forming a linear topology.
This topology is used for testing join security and routing features.
A sniffer is used for capturing the frames exchanged between DAGroot and 6TiSCH Node or 6TiSCH Node and other 6TiSCH Nodes.



                                  +----------------+
                                  |                |
                                  |    Sniffer     |
                                  |                |
                                  +----------------+


    +----------+         +-------------+         +-------------+         +-------------+
    |          | +-----> |             | +-----> |             | +-----> |             |
    | DAG root |         | 6TiSCH Node |         | 6TiSCH Node |         | 6TiSCH Node |
    |          | <-----+ |             | <-----+ |             | <-----+ |             |
    +----------+         +-------------+         +-------------+         +-------------+

#### Star

The star scenario includes 1 DAGroot and 2 6TiSCH Nodes, both directly connected to the DAGroot.
A sniffer is used for capturing the frames exchanged between DAGroot and 6TiSCH Nodes.
This star topology is mainly used for testing 6P features.

                          +----------------+
                          |                |
                          |    Sniffer     |
                          |                |
                          +----------------+


    +-------------+         +----------+         +-------------+
    |             | +-----> |          | +-----> |             |
    | 6TiSCH Node |         | DAG root |         | 6TiSCH Node |
    |             | <-----+ |          | <-----+ |             |
    +-------------+         +----------+         +-------------+

### The test description proforma

The test descriptions are provided in proforma tables, which includes the different steps of the Test Sequence.
The steps can be of different types, depending on their purpose:

- A **configure** corresponds to an action to modify the node under test configuration. It is abbreviated as **cfg** in proforma tables.
- A **stimulus** corresponds to an event that triggers a specific behavior on a node under test, such as  turning on/off the node under test or sending a message. It is abbreviated as **-s** in proforma tables. The users are responsible for triggering this stimulus.
- A **check** consists of observing that one node under test behaves as described in the standard: i.e. resource creation, update, deletion, etc. It is abbreviated as **-c** in proforma tables. It's the F-Interop Testing Tool that does the check during the interop event. You can also manually perform this check when preparing for the event. 
- The overall Verdict is considered "PASS" if and only if all the checks in the sequence are "PASS".

<a name="td"></a>

## Test description
Please note that the 6TiSCH Tests described hereafter are not F-Interop specific. Attendees should read the TD beforehand, and test their implementation again the Golden Device. During the event the F-Interop Platform will be performing the "check'' steps automatically.